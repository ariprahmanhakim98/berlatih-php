<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number >= 85 && $number <= 100){
    	$hasil = "Sangat Baik <br>";
    }else if($number >= 70 && $number < 85){
    	$hasil = "Baik <br>";
    }else if($number >= 60 && $number <70){
    	$hasil = "Cukup <br>";
    }else {
    	$hasil = "Kurang <br>";
    }
    return $hasil;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>